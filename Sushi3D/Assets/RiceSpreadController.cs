﻿using UnityEngine;

public class RiceSpreadController : MonoBehaviour
{
    public LayerMask layerMask;
    public Transform ricePivot;
    public GameObject riceAlpha;
    private GameObject alphaClone;
    private Vector3 alphaStartPos;

    [Header("DRAG AREA")]
    public LayerMask dragAreaMask;
    private Vector3 dragPosition;

    [Header("SUSHI ROLL")]
    public Animation sushiAnimation;
    [Range (0f, 1f)]public float rollController;





    void Start()
    {
        sushiAnimation.Play("RollAnimation");
        sushiAnimation["RollAnimation"].speed = 0;
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0)) 
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 1000, layerMask))
            {
                if (hit.collider != null)
                {
                    alphaClone = Instantiate(riceAlpha);
                    alphaStartPos = new Vector3(hit.point.x, ricePivot.position.y, hit.point.z);
                    alphaClone.transform.position = alphaStartPos;
                    alphaClone.transform.localEulerAngles = new Vector3(90,0,0);
                    alphaClone.transform.SetParent(ricePivot);
                }
            }
        }

        if (Input.GetMouseButton(0)) 
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 1000, dragAreaMask))
            {
                if (hit.collider != null)
                {
                    dragPosition = new Vector3(hit.point.x, ricePivot.position.y, hit.point.z);
                    if (Vector3.Distance(alphaStartPos, dragPosition) > 1.5f)
                    {
                        alphaClone = null;
                        alphaStartPos = Vector3.zero;
                        return;
                    }
                }
            }



            if (alphaClone != null)
            {
                alphaClone.transform.position = dragPosition;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            alphaClone = null;
            alphaStartPos = Vector3.zero;
        }

        //update roll animation
        sushiAnimation["RollAnimation"].normalizedTime = rollController;
    }
}
